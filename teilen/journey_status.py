from django.utils.translation import ugettext as _

JOURNEY_PENDING  = 1
JOURNEY_CANCELED = 2
JOURNEY_STARTED  = 3
JOURNEY_ENDED_OK = 4
JOURNEY_ENDED_KO = 5


JOURNEY_STATUS = (
    (JOURNEY_PENDING , _('pending'          )),
    (JOURNEY_CANCELED, _('canceled'         )),
    (JOURNEY_STARTED , _('started'          )),
    (JOURNEY_ENDED_OK, _('ended successfuly')),
    (JOURNEY_ENDED_KO, _('ended with issues')),
)
