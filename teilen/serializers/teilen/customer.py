from teilen.models  import Customer
from rest_framework import serializers

from teilen.serializers.filer  import image
from teilen.serializers.cities import city


class Serializer(serializers.ModelSerializer):
    city    = city .Serializer
    picture = image.Serializer

    class Meta:
        model  = Customer
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 'city', 'picture')


class DetailsSerializer(serializers.ModelSerializer):
    city    = city .DetailsSerializer()
    picture = image.Serializer()

    class Meta:
        model  = Customer
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 'city', 'picture')
