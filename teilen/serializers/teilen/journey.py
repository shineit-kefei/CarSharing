from teilen.models  import Journey
from rest_framework import serializers

from teilen.serializers.cities.city     import Serializer        as CitySerializer
from teilen.serializers.teilen.customer import Serializer        as CustomerSerializer

from teilen.serializers.cities.city     import DetailsSerializer as DetailsCitySerializer
from teilen.serializers.teilen.customer import DetailsSerializer as DetailsCustomerSerializer


class Serializer(serializers.ModelSerializer):
    driver         = CustomerSerializer
    arrival_city   = CitySerializer
    departure_city = CitySerializer

    class Meta:
        model  = Journey
        fields = (
            'id',
            'price',
            'places',
            'driver',
            'arrival_city',
            'arrival_date',
            'departure_city',
            'departure_date',
        )


class DetailsSerializer(serializers.ModelSerializer):
    driver         = DetailsCustomerSerializer()
    arrival_city   = DetailsCitySerializer()
    departure_city = DetailsCitySerializer()

    class Meta:
        model  = Journey
        fields = (
            'id',
            'price',
            'places',
            'driver',
            'arrival_city',
            'arrival_date',
            'departure_city',
            'departure_date',
        )
