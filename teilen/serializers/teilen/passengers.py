from teilen.models  import Passengers
from rest_framework import serializers

from teilen.serializers.teilen.journey  import Serializer        as JourneySerializer
from teilen.serializers.teilen.customer import Serializer        as CustomerSerializer

from teilen.serializers.teilen.journey  import DetailsSerializer as DetailsJourneySerializer
from teilen.serializers.teilen.customer import DetailsSerializer as DetailsCustomerSerializer


class Serializer(serializers.ModelSerializer):
    journey  = JourneySerializer
    customer = CustomerSerializer

    class Meta:
        model = Passengers


class DetailsSerializer(serializers.ModelSerializer):
    journey  = DetailsJourneySerializer()
    customer = DetailsCustomerSerializer()

    class Meta:
        model = Passengers
