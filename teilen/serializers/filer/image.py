from filer.models   import Image
from rest_framework import serializers


class Serializer(serializers.ModelSerializer):
    url    = serializers.URLField    ()
    size   = serializers.IntegerField()
    width  = serializers.IntegerField()
    label  = serializers.CharField   ()
    height = serializers.IntegerField()

    class Meta:
        model  = Image
        fields = ('id', 'name', 'url', 'size', 'width', 'height')
