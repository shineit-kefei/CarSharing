from cities.models  import City
from rest_framework import serializers

from teilen.serializers.cities import region


class Serializer(serializers.ModelSerializer):
    region = region.Serializer

    class Meta:
        model  = City
        fields = ('id', 'name', 'name_std', 'region')


class DetailsSerializer(serializers.ModelSerializer):
    parent = serializers.RelatedField()
    region = region.DetailsSerializer()

    class Meta:
        model  = City
        fields = ('id', 'name', 'name_std', 'parent', 'region')
