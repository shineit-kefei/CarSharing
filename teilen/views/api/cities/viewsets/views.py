from cities.models     import City
from cities.models     import Region
from cities.models     import Country

from rest_framework.response    import Response
from rest_framework.viewsets    import ModelViewSet
from rest_framework.renderers   import JSONRenderer
from rest_framework.permissions import IsAuthenticated

from teilen.serializers.cities.city    import Serializer as CitySerializer
from teilen.serializers.cities.region  import Serializer as RegionSerializer
from teilen.serializers.cities.country import Serializer as CountrySerializer


class Cities(ModelViewSet):
    queryset           = City.objects.all()
    serializer_class   = CitySerializer
    renderer_classes   = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def detail(self, request, pk, *args, **kwargs):
        return Response({"extra": True})


class Regions(ModelViewSet):
    queryset           = Region.objects.all()
    serializer_class   = RegionSerializer
    renderer_classes   = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def details(self, request, pk, *args, **kwargs):
        return Response({"extra": True})


class Countries(ModelViewSet):
    queryset           = Country.objects.all()
    serializer_class   = CountrySerializer
    renderer_classes   = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def detail(self, request, pk, *args, **kwargs):
        return Response({"extra": True})
