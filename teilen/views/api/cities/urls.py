from django.conf.urls import url
from django.conf.urls import patterns

from teilen.views.api.cities.views import CitiesList
from teilen.views.api.cities.views import RegionsList
from teilen.views.api.cities.views import CountriesList

from teilen.views.api.cities.views import CitiesAuto
from teilen.views.api.cities.views import RegionsAuto
from teilen.views.api.cities.views import CountriesAuto

from teilen.views.api.cities.views import CitiesDetail
from teilen.views.api.cities.views import RegionsDetail
from teilen.views.api.cities.views import CountriesDetail

from teilen.views.api.cities.views import CitiesRetrieve
from teilen.views.api.cities.views import RegionsRetrieve
from teilen.views.api.cities.views import CountriesRetrieve


urlpatterns = patterns(
    '',
    url(r'^city/$'                      , CitiesList       , name='cities_list_api'       ),
    url(r'^region/$'                    , RegionsList      , name='regions_list_api'      ),
    url(r'^country/$'                   , CountriesList    , name='countries_list_api'    ),

    url(r'^city/auto/$'                 , CitiesAuto       , name='cities_auto_api'       ),
    url(r'^region/auto/$'               , RegionsAuto      , name='regions_auto_api'      ),
    url(r'^country/auto/$'              , CountriesAuto    , name='countries_auto_api'    ),

    url(r'^city/(?P<pk>\d+)/$'          , CitiesRetrieve   , name='cities_retrieve_api'   ),
    url(r'^region/(?P<pk>\d+)/$'        , RegionsRetrieve  , name='regions_retrieve_api'  ),
    url(r'^country/(?P<pk>\d+)/$'       , CountriesRetrieve, name='countries_retrieve_api'),

    url(r'^city/(?P<pk>\d+)/detail/$'   , CitiesDetail     , name='cities_detail_api'     ),
    url(r'^region/(?P<pk>\d+)/detail/$' , RegionsDetail    , name='regions_detail_api'    ),
    url(r'^country/(?P<pk>\d+)/detail/$', CountriesDetail  , name='countries_detail_api'  ),
)
