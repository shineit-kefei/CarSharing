from teilen.views.api.teilen.viewsets.views     import *
from teilen.views.api.teilen.autocomplete.views import *


ProfileList         = Profile              .as_view({'get': 'list'    })
CustomersList       = Customers            .as_view({'get': 'list'    })
CustomersAuto       = CustomersAutocomplete.as_view(                   )
CustomersDetail     = Customers            .as_view({'get': 'detail'  })
CustomersRetrive    = Customers            .as_view({'get': 'retrieve'})
ProfileSharedList   = ProfileShared        .as_view({'get': 'list'    })
ProfileJourneysList = ProfileJourneys      .as_view({'get': 'list'    })
