import os

from rest_framework.status    import HTTP_201_CREATED
from rest_framework.status    import HTTP_400_BAD_REQUEST
from rest_framework.views     import APIView
from rest_framework.response  import Response
from rest_framework.renderers import JSONRenderer

from filer.models          import Image
from filer.models          import Folder
from cities.models         import City
from django.db.models      import Q
from teilen.models         import Journey
from teilen.models         import Customer
from teilen.models         import Passengers
from teilen.journey_status import JOURNEY_PENDING


from datetime                   import datetime
from django.conf                import settings
from django.core.mail           import send_mail
from django.utils.http          import int_to_base36
from django.template.loader     import render_to_string
from django.utils.translation   import ugettext as _
from django.contrib.auth.tokens import PasswordResetTokenGenerator

from teilen.serializers.teilen.customer import Serializer as CustomerSerializer


class Registration_api(APIView):
    renderer_classes = (JSONRenderer,)

    def post(self, request, *args, **kwargs):
        try:
            # city = City.objects.get(id=request.REQUEST['city_id'])
            # picture = Image.objects.get(id=request.REQUEST['photo_id'])
            # user = Customer.objects.create(
            #     city=city,
            #     picture=picture,
            #     email=request.REQUEST['email'],
            #     username=request.REQUEST['user_name'],
            #     last_name=request.REQUEST['last_name'],
            #     first_name=request.REQUEST['first_name'],
            # )
            serializer = CustomerSerializer(data=request.DATA)
            if serializer.is_valid():
                user = serializer.save()
                user.set_password(request.REQUEST['password'])
                user.save()
                return Response(serializer.data, status=HTTP_201_CREATED)
            return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
            #return Response({'status': True, 'message': _('Customer added successfuly')})
        except Exception as e:
            #return Response({'status': False, 'message': str(e)})
            return Response(str(e), status=HTTP_400_BAD_REQUEST)


class PictureUpload_api(APIView):
    renderer_classes = (JSONRenderer,)

    def post(self, request, *args, **kwargs):
        now = datetime.now()
        from django.core.files.storage import default_storage
        from django.core.files.base import ContentFile
        data = request.FILES['photo']

        filer_path = settings.FILER_STORAGES['public']['main']['OPTIONS']['location']
        relpath = "filer_public/{0}/{1:0>2}/{2:0>2}/{3}".format(now.year, now.month, now.day, data.name)
        abspath = os.path.abspath(os.path.join(filer_path, relpath))

        if not os.path.exists(os.path.dirname(abspath)):
            os.makedirs(os.path.dirname(abspath))

        path = default_storage.save(abspath, ContentFile(data.read()))
        path = path.replace(filer_path, "")
        folder, created = Folder.objects.get_or_create(name='customers')
        image, created  = Image.objects.get_or_create(file=(path[1:] if path[0] == '/' else path))
        image.name      = data.name
        image.folder    = folder
        image.save()
        return Response(
            {
                'id' : image.id,
                'url': image.url
            })


class ResetPassword_api(APIView):
    renderer_classes = (JSONRenderer,)

    def post(self, request, *args, **kwargs):
        email = request.REQUEST['email']
        try:
            user = Customer.objects.get(email=email)

            email_configuration = {
                'uid'      : int_to_base36(user.pk),
                'user'     : user,
                'email'    : user.email,
                'token'    : PasswordResetTokenGenerator.make_token(user),
                'domain'   : "example.com",
                'site_name': "car sharing inc",
                'protocol' : 'https',
            }
            send_mail(
                _('Email password reset'),
                render_to_string("reset_email", email_configuration),
                "admin@example.com",
                user.email
            )

            return Response({'status': True, 'email': user.email, 'message': _('a mail will be sent to you in a short time')})
        except Exception:
            return Response({'status': False, 'email': user.email, 'message': _('this email is not associated with any user')})


class Search_api(APIView):
    renderer_classes = (JSONRenderer,)

    def post(self, request, *args, **kwargs):
        status  = True
        message = ''
        results = []

        arrival_city   = request.REQUEST['arrival_city_id'  ]
        departure_city = request.REQUEST['departure_city_id']
        arrival_date   = datetime.strptime(request.REQUEST['arrival_date'  ], "%m/%d/%Y").date()
        departure_date = datetime.strptime(request.REQUEST['departure_date'], "%m/%d/%Y").date()
        try:
            if 'strict_mode' in request.REQUEST:
                journeys = Journey.objects.filter(
                   ~Q(driver                   = self.request.user)).filter(
                    arrival_city__id           = arrival_city,
                    departure_city__id         = departure_city,
                    arrival_date__startswith   = arrival_date,
                    departure_date__startswith = departure_date,
                )
            else:
                journeys = Journey.objects.filter(
                   ~Q(driver              =self.request.user              )&
                    Q(arrival_city__id     =arrival_city                  )&
                    Q(departure_city__id   =departure_city                )).filter(
                    Q(arrival_date__range  =(departure_date, arrival_date))|
                    Q(departure_date__range=(departure_date, arrival_date))
                )
            for journey in journeys:
                results.append({
                    'id'               : journey.id,
                    'price'            : journey.price,
                    'total_places'     : journey.places,
                    'arrival_date'     : journey.arrival_date,
                    'departure_date'   : journey.departure_date,
                    'driver_last_name' : journey.driver.last_name,
                    'available_places' : journey.available_places,
                    'driver_first_name': journey.driver.first_name,
                })
        except Exception as e:
            status  = False
            message = str(e)

        return Response({
            'status'  : status,
            'message' : message,
            'journeys': results,
        })


class Reservation_api(APIView):
    renderer_classes = (JSONRenderer,)

    def post(self, request, *args, **kwargs):
        status  = True
        message = ''

        try:
            journey   = Journey.objects.get(id=request.REQUEST["journey_id"])
            customer  = self.request.user
            operation = request.REQUEST["operation"]
            if operation is True and journey.available_places > 1:
                journey.passengers.add(customer)
                journey.save()
            elif operation is False and customer in journey.passengers.all():
                journey.passengers.remove(customer)
            else:
                status  = False
                message = _("Invalid request")
        except Exception as e:
            status  = False
            message = str(e)
        return Response({
            'status'  : status,
            'message' : message,
        })


class Profile_api(APIView):
    renderer_classes = (JSONRenderer,)

    def post(self, request, *args, **kwargs):
        status  = True
        message = ''
        result  = {
            'shares'  : [],
            'profile' : {},
            'journeys': [],
        }
        try:
            customer = self.request.user
            shared   = Journey.objects.filter(driver=customer)
            jouneys  = Passengers.objects.filter(customer=customer)

            result['profile'] = {
                'city'      : customer.city.__unicode__(),
                'email'     : customer.email,
                'picture'   : customer.picture.url,
                'username'  : customer.username,
                'last_name' : customer.last_name,
                'first_name': customer.first_name,
            }

            for share in shared:
                result['shares'].append({
                    'price'            : share.price,
                    'driver_last_name' : share.driver.last_name,
                    'driver_first_name': share.driver.first_name,
                    'arrival_date'     : share.arrival_date,
                    'arrival_city'     : share.arrival_city.__unicode__(),
                    'departure_date'   : share.departure_date,
                    'departure_city'   : share.departure_city.__unicode__(),
                })
            for journey in jouneys:
                result['journeys'].append({
                    'price'            : journey.price,
                    'driver_last_name' : journey.driver.last_name,
                    'driver_first_name': journey.driver.first_name,
                    'arrival_date'     : journey.arrival_date,
                    'arrival_city'     : journey.arrival_city.__unicode__(),
                    'departure_date'   : journey.departure_date,
                    'departure_city'   : journey.departure_city.__unicode__(),
                })
        except Exception as e:
            status  = False
            message = str(e)
        return Response({
            'status'  : status,
            'result'  : result,
            'message' : message,
        })


class Share_api(APIView):
    renderer_classes = (JSONRenderer,)

    def post(self, request, *args, **kwargs):
        status  = True
        message = ''

        try:
            Journey.objects.create(
                price=request.REQUEST['share_price'],
                status=JOURNEY_PENDING,
                driver=self.request.user,
                places=request.REQUEST['share_places'],
                arrival_date=datetime.strptime(request.REQUEST['share_arrival_date'], "%m/%d/%Y"),
                departure_date=datetime.strptime(request.REQUEST['share_departure_date'], "%m/%d/%Y"),
                arrival_city=City.objects.get(id=request.REQUEST['share_arrival_city_id']),
                departure_city=City.objects.get(id=request.REQUEST['share_departure_city_id']),
            )
        except Exception as e:
            status  = False
            message = str(e)
        return Response({
            'status'  : status,
            'message' : message,
        })
