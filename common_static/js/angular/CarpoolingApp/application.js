var CarpoolingApp = angular.module('CarpoolingApp', ['ui.bootstrap']);

CarpoolingApp.config(
	function($interpolateProvider) {
		$interpolateProvider.endSymbol  (']]');
		$interpolateProvider.startSymbol('[[');
});

CarpoolingApp.controller(
	'ProfileInfoCtrl', [
		'$scope', '$http' ,
		function($scope, $http) {
			$scope.rate       = 7   ;
			$scope.max        = 10  ;
			$scope.isReadonly = true;

			$scope.hoveringOver = function(value) {
				$scope.overStar = value;
				$scope.percent = 100 * (value / $scope.max);
			};

			$scope.ratingStates = [
				{stateOn : 'glyphicon-ok-sign', stateOff: 'glyphicon-ok-circle' },
				{stateOn : 'glyphicon-star'   , stateOff: 'glyphicon-star-empty'},
				{stateOn : 'glyphicon-heart'  , stateOff: 'glyphicon-ban-circle'},
				{stateOn : 'glyphicon-heart'                                    },
				{stateOff: 'glyphicon-off'                                      }
			];

			$http.get('/api/teilens/profile/').
				success(function(data) {
					$scope.profile = data[0];
})}]);

CarpoolingApp.controller(
	'ProfileSharedCtrl', [
		'$scope', '$http' ,
		function($scope, $http) {
			$('#profile_shared_table').footable();
			$('#profile_shared_table tbody').innerHTML='';

			$http.get('/api/teilens/profile/shared/').
				success(function(data) {
					$scope.shared_journeys = data;
})}]);


CarpoolingApp.controller(
	'ProfileJourneysCtrl', [
		'$scope', '$http' ,
		function($scope, $http) {
			$('#profile_journeys_table').footable();
			$('#profile_journeys_table tbody').innerHTML='';

			$http.get('/api/teilens/profile/journeys/').
				success(function(data) {
					$scope.journeys = data;
})}]);